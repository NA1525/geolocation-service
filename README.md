# Geolocation Service

This application provides Geolocation information based on IP address

**Libraries used**
 
1)Java 11, Spring Boot 2.4.3.RELEASE
2)Open API
3)Apache Commons
4)MongoDB

**Features**

This service can perform following operations

1) Upload the csv file with Geolocation data.
2) Retrieve  Geolocation information based on IP Address.

**Setting up application**

Perform below steps to start application in docker. 

1) Build the application using maven in project directory  -- mvn clean install
3) Build docker-compose.yml          -- docker-compose build
4) Start docker-compose              -- docker-compose up

Note : If user wants to run application locally - without docker , than mongo daemon on local should be started and change spring.data.mongodb.host to localhost in application.properties. This will allow user to run this application locally with port 8080

**Test application deployed on Local with Docker**

Swagger API specification

http://localhost:8091/geolocation-ws/swagger-ui/index.html

if it does not populate gelocation service by default, paste below in explore section of the page

http://localhost:8091/geolocation-ws/api-docs

please try "/v1/upload" to upload the desired data file.
use "/v1/geolocation"  to get the Geolocation information.

**Test application deployed on AWS**

Swagger API specification

http://13.53.174.101:8091/geolocation-ws/swagger-ui/index.html

if it does not populate gelocation service by default, paste below in explore section of the page

http://13.53.174.101:8091/geolocation-ws/api-docs



**Versions**

1.0.0-SNAPSHOT

**Developers**

Mitesh Nakrani (mnakrani04051990@gmail.com)