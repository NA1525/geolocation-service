FROM openjdk:11
MAINTAINER mnakrani04051990@gmail.com
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} geolocation-service.jar
ENTRYPOINT ["java","-jar","/geolocation-service.jar"]