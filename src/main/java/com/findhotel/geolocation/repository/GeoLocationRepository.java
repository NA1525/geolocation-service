package com.findhotel.geolocation.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import com.findhotel.geolocation.constants.GeoLocationConstants;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.mongodb.BasicDBObject;

@Repository
public class GeoLocationRepository {
	@Autowired
	private MongoTemplate mongoTemplate;

	public void setup() {
		mongoTemplate.dropCollection(GeoLocationConstants.DB_COLLECTION); // In case of re-import of the csv file
		BasicDBObject index = new BasicDBObject();
		index.put(GeoLocationConstants.IPADDRESS, 1); // Creating index on IP address for better performance
		mongoTemplate.getCollection(GeoLocationConstants.DB_COLLECTION).createIndex(index);
	}

	public void save(GeoLocationModel geoLocationModel) {
		mongoTemplate.save(geoLocationModel);
	}

	public GeoLocationModel getGeolocationbyIPAddress(String ipAddress) {
		Query query = new Query();
		query.addCriteria(Criteria.where(GeoLocationConstants.IPADDRESS).is(ipAddress));
		query.fields().exclude(GeoLocationConstants.ID); // Exclude field not required in the response
		return mongoTemplate.findOne(query, GeoLocationModel.class);
	}
}