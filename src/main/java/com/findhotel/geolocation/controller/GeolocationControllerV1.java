/**
 * 
 */
package com.findhotel.geolocation.controller;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.findhotel.geolocation.helper.GeoLocationHelper;
import com.findhotel.geolocation.message.ResponseBody;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.service.GeoLocationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * This class is providing a Http interface to interact with Geolocation
 * application
 *
 */
@RestController
@Slf4j
@Api(value = "Geolocation Service")
@RequestMapping("/v1")
public class GeolocationControllerV1 {

	@Autowired
	GeoLocationService geoLocationService;

	@Autowired
	GeoLocationHelper geoLocationHelper;

	@ApiOperation(value = "This API gets the geolocation data based on ip address")
	@GetMapping("geolocation/{ipAddress}")
 	public ResponseEntity<Object> getGeolocationData(@PathVariable @NotBlank String ipAddress) {
		GeoLocationModel geoLocationModel = null;
		try {
			if (geoLocationHelper.validateIpAddress(ipAddress)) {
				return new ResponseEntity<>(ResponseBody.builder().message("Invalid input IP address format").build(), HttpStatus.BAD_REQUEST);
			}
			geoLocationModel = geoLocationService.findGeolocationbyIPAddress(ipAddress);
			if (geoLocationModel != null) {
				return new ResponseEntity<>(geoLocationModel, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(ResponseBody.builder().message("IP address not found").build(), HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("Error while fetching Geolocation Data : {} ", e);
				return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@ApiOperation(value = "This API upload geolocation data")
	@PostMapping(value = "upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<ResponseBody> uploadFile(@RequestParam("file") @NotNull MultipartFile file) {
		String message = "";
		GeoLocationDataStatistics geoLocationDataStatistics = null;
		Long startTime = System.currentTimeMillis();

		if (geoLocationHelper.hasCSVFormat(file)) {
			try {
				geoLocationDataStatistics = geoLocationService.saveRecord(file);
				log.info("Data upload finished .. ");
				geoLocationDataStatistics.setTimeElapsed((System.currentTimeMillis() - startTime) / 1000 + " Seconds");
				message = "File uploaded successfully !";
				return ResponseEntity.status(HttpStatus.OK).body(
						ResponseBody.builder().dataStatistics(geoLocationDataStatistics).message(message).build());
			} catch (Exception e) {
				message = "Error while trying to upload data : {} ";
				log.error(message, e);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
						.body(ResponseBody.builder().message(message).build());
			}
		}
		message = "Please upload a csv file!";
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseBody.builder().message(message).build());
	}
}