package com.findhotel.geolocation.model;

import java.io.Serializable;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;

@Document(collection = "GEOLOCATION_DATA")
@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class GeoLocationModel implements Serializable {
	private static final long serialVersionUID = 1L;
	private String ipAddress;
	private String countryCode;
	private String country;
	private String city;
	private Double latitude;
	private Double longitude;
	private String mysteryValue;
}