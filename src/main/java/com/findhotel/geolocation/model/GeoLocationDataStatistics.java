package com.findhotel.geolocation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class GeoLocationDataStatistics {
	private String timeElapsed;
	private int totalRecords;
	private int recordsSaved;
	private int duplicateRecords;
	private int invalidRecords;
}