package com.findhotel.geolocation.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(Include.NON_NULL)
public class ResponseBody {
	private String message;
	private GeoLocationDataStatistics dataStatistics;
}