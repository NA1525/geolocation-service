package com.findhotel.geolocation.constants;

import java.util.regex.Pattern;

public class GeoLocationConstants {
	public static String IP_ADDRESS = "ip_address";
	public static String COUNTRY_CODE = "country_code";
	public static String COUNTRY = "country";
	public static String CITY = "city";
	public static String LATITUDE = "latitude";
	public static String LONGITUDE= "longitude";
	public static String MYSTERY_VALUE = "mystery_value";
	public static String ID = "_id";
	public static String DB_COLLECTION = "GEOLOCATION_DATA";
	public static String IPADDRESS = "ipAddress";
	public static String TYPE = "application/vnd.ms-excel";
	public static String DOT = "\\.";
	public static String UTF_8 = "UTF-8";
	public static String ZERO_TO_255 = "(\\d{1,2}|(0|1)\\" + "d{2}|2[0-4]\\d|25[0-5])";
	public static Pattern IP_ADDRESS_REGEX = Pattern
			.compile(ZERO_TO_255 +DOT + ZERO_TO_255 + DOT + ZERO_TO_255 + DOT + ZERO_TO_255);  //Regular expression for ip address
}