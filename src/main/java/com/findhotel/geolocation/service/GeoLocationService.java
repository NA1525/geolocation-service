package com.findhotel.geolocation.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.findhotel.geolocation.helper.GeoLocationHelper;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.repository.GeoLocationRepository;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GeoLocationService {
	@Autowired
	public GeoLocationRepository repository;

	@Autowired
	public GeoLocationHelper geoLocationHelper;

	public GeoLocationDataStatistics saveRecord(MultipartFile file) {
		try {
			log.info("Started Data uploading .. ");
			return geoLocationHelper.mapGeolocationData(this, file.getInputStream());
		} catch (IOException e) {
			log.error("Error while parsing the csv data");
			throw new RuntimeException("fail to store csv data : " + e.getMessage());
		}
	}

	public GeoLocationModel findGeolocationbyIPAddress(String ipAddress) {
		return repository.getGeolocationbyIPAddress(ipAddress);
	}
}