package com.findhotel.geolocation.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.findhotel.geolocation.constants.GeoLocationConstants;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.repository.GeoLocationRepository;
import com.findhotel.geolocation.service.GeoLocationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GeoLocationHelper {

	@Autowired
	public GeoLocationRepository repository;

	public boolean hasCSVFormat(MultipartFile file) {
		if (GeoLocationConstants.TYPE.equals(file.getContentType())) {
			return true;
		}
		return false;
	}

	public boolean validateCountryCode(String countryCode) {
		return StringUtils.isEmpty(countryCode) || countryCode.length() != 2 || !countryCode.matches("^[a-zA-Z]*$");
	}

	public GeoLocationDataStatistics populateDataStatistics(int recordsSaved, int totalRecords,
			int dataDuplicateCount) {
		log.info("Geo location records saved : {} ", recordsSaved);
		log.info("total records removed : {} ", totalRecords - recordsSaved);
		return GeoLocationDataStatistics.builder().totalRecords(totalRecords)
				.invalidRecords(totalRecords - recordsSaved - dataDuplicateCount).recordsSaved(recordsSaved)
				.duplicateRecords(dataDuplicateCount).build();
	}

	public boolean validateIpAddress(String ipsaddress) {
		return StringUtils.isEmpty(ipsaddress) || !GeoLocationConstants.IP_ADDRESS_REGEX.matcher(ipsaddress).matches();
	}

	public void saveGeolocationData(String ipsAddress, String countryCode, String country, String city, String latitude,
			String longitude, CSVRecord csvRecord) {
		repository.save(GeoLocationModel.builder().ipAddress(ipsAddress).countryCode(countryCode).country(country)
				.city(city).latitude(Double.valueOf(latitude)).longitude(Double.valueOf(longitude))
				.mysteryValue(csvRecord.get(GeoLocationConstants.MYSTERY_VALUE)).build());
	}

	public GeoLocationDataStatistics mapGeolocationData(GeoLocationService geoLocationService, InputStream csvfile)
			throws IOException {
		try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(csvfile, GeoLocationConstants.UTF_8));
				CSVParser csvParser = new CSVParser(fileReader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

			Set<String> ipaddressSet = new HashSet<>();
			String ipsAddress = null;
			String countryCode = null;
			String country = null;
			String city = null;
			String latitude = null;
			String longitude = null;
			CSVRecord csvRecord = null;
			int dataDuplicateCount = 0;
			int totalRecords = 0;
			while (csvParser.iterator().hasNext()) {
				csvRecord = csvParser.iterator().next();
				totalRecords++;
				ipsAddress = csvRecord.get(GeoLocationConstants.IP_ADDRESS);

				if (ipaddressSet.contains(ipsAddress)) {
					dataDuplicateCount++;
				} else {
					if (validateIpAddress(ipsAddress)) {
						continue;
					}
					countryCode = csvRecord.get(GeoLocationConstants.COUNTRY_CODE);
					if (validateCountryCode(countryCode)) {
						continue;
					}
					country = csvRecord.get(GeoLocationConstants.COUNTRY);
					city = csvRecord.get(GeoLocationConstants.CITY);
					if (StringUtils.isEmpty(country) || StringUtils.isEmpty(city)) {
						continue;
					}
					latitude = csvRecord.get(GeoLocationConstants.LATITUDE);
					longitude = csvRecord.get(GeoLocationConstants.LONGITUDE);
					if (!NumberUtils.isCreatable(latitude) || !NumberUtils.isCreatable(longitude)) {
						continue;
					}
					ipaddressSet.add(ipsAddress);
					saveGeolocationData(ipsAddress, countryCode, country, city, latitude, longitude, csvRecord);
				}
			}
			return populateDataStatistics(ipaddressSet.size(), totalRecords, dataDuplicateCount);
		}
	}
}