package com.findhotel.geolocation.service.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.file.Paths;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import com.findhotel.geolocation.constants.GeoLocationConstants;
import com.findhotel.geolocation.controller.test.GeolocationControllerV1Test;
import com.findhotel.geolocation.helper.GeoLocationHelper;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.repository.GeoLocationRepository;
import com.findhotel.geolocation.service.GeoLocationService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GeoLocationServiceTest {

	@Autowired
	GeoLocationHelper geoLocationHelper;
	
	@MockBean
	GeoLocationRepository geoLocationRepository;
	
	@Autowired 
	GeoLocationService geoLocationService;
	
	@Test
	public void testFindGeolocationbyIPAddress() {
		when(geoLocationRepository.getGeolocationbyIPAddress(anyString())).thenReturn(getGeoLocationObject());
		GeoLocationModel geoLocationModel = geoLocationService.findGeolocationbyIPAddress("200.106.141.15");
		assertNotNull(geoLocationModel);
		assertEquals("Amsterdam", geoLocationModel.getCity());
		assertEquals("Netherlands", geoLocationModel.getCountry());
		assertEquals("NL", geoLocationModel.getCountryCode());
	}
	
	@Test
	public void testFindGeolocationbyIPAddressNegativeFlow() {
		when(geoLocationRepository.getGeolocationbyIPAddress(anyString())).thenReturn(null);
		GeoLocationModel geoLocationModel = geoLocationService.findGeolocationbyIPAddress("200.106.141.15");
		assertNull(geoLocationModel);
	}
	
	@Test
	public void testSaveRecordPositiveFlow() throws Exception {
		URL resource = GeolocationControllerV1Test.class.getResource("/data_dump_test.csv");
		String absolutePath = Paths.get(resource.toURI()).toFile().getAbsolutePath();
		FileInputStream fis = new FileInputStream(absolutePath);
		MockMultipartFile multipartFile = new MockMultipartFile("file","file",GeoLocationConstants.TYPE, fis);
		GeoLocationDataStatistics dataStatistics = geoLocationService.saveRecord(multipartFile);
		assertNotNull(dataStatistics);
		assertEquals(10,dataStatistics.getTotalRecords());  // 10 records in the file , 8 saved , 1 duplicate and 1 invalid.
		assertEquals(8,dataStatistics.getRecordsSaved());
		assertEquals(1,dataStatistics.getDuplicateRecords());
		assertEquals(1,dataStatistics.getDuplicateRecords());
	}
	
	private GeoLocationModel getGeoLocationObject() {
		return GeoLocationModel.builder().ipAddress("200.106.141.15").countryCode("NL").country("Netherlands")
				.city("Amsterdam").latitude(-84.87503094689836).longitude(7.206435933364332)
				.mysteryValue("7823011346").build();
	}
}
