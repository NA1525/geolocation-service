package com.findhotel.geolocation.repository.test;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

@EnableMongoRepositories("com.findhotel")
@ComponentScan("com.findhotel")
public class DatabaseConfig {
	
	String mongoUri = "mongodb://localhost:27017";
	public MongoDatabaseFactory createMongoDBFactory() {
		return new SimpleMongoClientDatabaseFactory(mongoUri);
	}
	
	@Bean
	public MongoDatabase db(MongoDatabaseFactory databaseFactory) {
		return databaseFactory.getMongoDatabase();
	}
	
	@Bean
	public MongoDatabase mongoDataBase(MongoClient mongoClient) {
		return mongoClient.getDatabase("test");
	}
}