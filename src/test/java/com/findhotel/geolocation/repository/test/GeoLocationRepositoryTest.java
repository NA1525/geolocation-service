package com.findhotel.geolocation.repository.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.bson.Document;
import org.junit.After;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import com.findhotel.geolocation.constants.GeoLocationConstants;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.repository.GeoLocationRepository;
import com.mongodb.client.ListIndexesIterable;

@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(loader = AnnotationConfigWebContextLoader.class, classes = { DatabaseConfig.class})
public class GeoLocationRepositoryTest {
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired GeoLocationRepository geoLocationRepository;
	
	@After 
	public void tearDown() {mongoTemplate.dropCollection(GeoLocationConstants.DB_COLLECTION);}
	
	@Test
	public void saveGeoLocationTest() {
		GeoLocationModel geoLocationModel = getGeoLocationObject();
		geoLocationRepository.save(geoLocationModel);  //Saving data in mongoDB
		geoLocationModel = geoLocationRepository.getGeolocationbyIPAddress("200.106.141.15");  // Try to retrieve Data with same IP Address
		assertNotNull(geoLocationModel);
		assertEquals("Amsterdam", geoLocationModel.getCity());
		assertEquals("Netherlands", geoLocationModel.getCountry());
		assertEquals("NL", geoLocationModel.getCountryCode());
	}
	
	@Test
	public void getInvalidIPAddressNotReturningResultTest() {
		GeoLocationModel geoLocationModel = geoLocationRepository.getGeolocationbyIPAddress("100.1.1.1");
		assertNull(geoLocationModel);
	}
	
	@Test
	public void testSetupIndexCreation() {
		geoLocationRepository.setup(); // This will create index
		boolean indexValue = false;
		ListIndexesIterable<Document> indexesIterable = mongoTemplate.getCollection(GeoLocationConstants.DB_COLLECTION).listIndexes();
	    for (Document index : indexesIterable) {
	        Document key = (Document) index.get("key");
	        if (key.containsKey(GeoLocationConstants.IPADDRESS)) {
	        	indexValue = true;
	        }
	    }
		assertTrue(indexValue);  //Index successfully created.
	}

	private GeoLocationModel getGeoLocationObject() {
		return GeoLocationModel.builder().ipAddress("200.106.141.15").countryCode("NL").country("Netherlands")
				.city("Amsterdam").latitude(-84.87503094689836).longitude(7.206435933364332)
				.mysteryValue("7823011346").build();
	}
}