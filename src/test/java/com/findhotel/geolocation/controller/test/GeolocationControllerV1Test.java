package com.findhotel.geolocation.controller.test;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.file.Paths;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.findhotel.geolocation.constants.GeoLocationConstants;
import com.findhotel.geolocation.controller.GeolocationControllerV1;
import com.findhotel.geolocation.helper.GeoLocationHelper;
import com.findhotel.geolocation.model.GeoLocationDataStatistics;
import com.findhotel.geolocation.model.GeoLocationModel;
import com.findhotel.geolocation.service.GeoLocationService;

@WebMvcTest(GeolocationControllerV1.class)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = GeolocationControllerV1TestConfig.class)
public class GeolocationControllerV1Test {
	
	@MockBean
	GeoLocationService geoLocationService;
	
	@Autowired
	GeoLocationHelper geoLocationHelper;
	
	@MockBean
	MongoTemplate mongoTemplate;
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	@Autowired
	private MockMvc mockmvc;
	
	@Before
	public void setup() {
		mockmvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build(); 
	}
	
	@Test
	public void getGeolocationForInvalidIPFormatTest() throws Exception{
		mockmvc.perform(get("/v1/geolocation/1001a.106.141.15"))
				.andExpect(status().isBadRequest())
				.andExpect(content().json("{\r\n" + 
						"    \"message\": \"Invalid input IP address format\"\r\n" + 
						"}"));		
	}
	
	@Test
	public void getGeolocationForValidIPButNotPresentTest() throws Exception{
		when(geoLocationService.findGeolocationbyIPAddress(anyString())).thenReturn(null);
		mockmvc.perform(get("/v1/geolocation/{ipaddress}","101.106.141.15"))
				.andExpect(status().isNotFound())
				.andExpect(content().json("{\r\n" + 
						"    \"message\": \"IP address not found\"\r\n" + 
						"}"));		
	}
	
	@Test
	public void getGeolocationInfoTest() throws Exception{
		when(geoLocationService.findGeolocationbyIPAddress(anyString())).thenReturn(getGeoLocationObject());
		this.mockmvc.perform(get("/v1/geolocation/200.106.141.15"))
				.andExpect(status().isOk())
				.andExpect(content().json("{\r\n" + 
						"    \"ipAddress\": \"200.106.141.15\",\r\n" + 
						"    \"countryCode\": \"NL\",\r\n" + 
						"    \"country\": \"Netherlands\",\r\n" + 
						"    \"city\": \"Amsterdam\",\r\n" + 
						"    \"latitude\": -84.87503094689836,\r\n" + 
						"    \"longitude\": 7.206435933364332,\r\n" + 
						"    \"mysteryValue\": \"7823011346\"\r\n" + 
						"}"));		
	}
	
	@Test
	public void uploadGeoLocationDataTest() throws Exception{
		URL resource = GeolocationControllerV1Test.class.getResource("/data_dump_test.csv");
		String absolutePath = Paths.get(resource.toURI()).toFile().getAbsolutePath();
		FileInputStream fis = new FileInputStream(absolutePath);
		MockMultipartFile multipartFile = new MockMultipartFile("file","file",GeoLocationConstants.TYPE, fis);
		when(geoLocationService.saveRecord(any())).thenReturn(geoLocationDataStatistics());
		
		MockHttpServletRequestBuilder builder = 
				MockMvcRequestBuilders.multipart("/v1/upload")
				  .file(multipartFile)
				  .content(MediaType.MULTIPART_FORM_DATA_VALUE);
		this.mockmvc.perform(builder).
		    andExpect(status().is(200));
	}
	
	@Test
	public void uploadInvalidCSVDataTest() throws Exception{
		URL resource = GeolocationControllerV1Test.class.getResource("/invalid_data_dump.txt");
		String absolutePath = Paths.get(resource.toURI()).toFile().getAbsolutePath();
		FileInputStream fis = new FileInputStream(absolutePath);
		MockMultipartFile multipartFile = new MockMultipartFile("file","file","txt", fis);
		when(geoLocationService.saveRecord(any())).thenReturn(geoLocationDataStatistics());
		
		MockHttpServletRequestBuilder builder = 
				MockMvcRequestBuilders.multipart("/v1/upload")
				  .file(multipartFile)
				  .content(MediaType.MULTIPART_FORM_DATA_VALUE);
		this.mockmvc.perform(builder).
		    andExpect(status().is(400)).
		    andExpect(content().json("{}"));
	}
	
	private GeoLocationDataStatistics geoLocationDataStatistics() {
		return GeoLocationDataStatistics.builder().totalRecords(10)
				.invalidRecords(1).recordsSaved(8)
				.duplicateRecords(1).build();
	}
	
	private GeoLocationModel getGeoLocationObject() {
		return GeoLocationModel.builder().ipAddress("200.106.141.15").countryCode("NL").country("Netherlands")
				.city("Amsterdam").latitude(-84.87503094689836).longitude(7.206435933364332)
				.mysteryValue("7823011346").build();
	}
}